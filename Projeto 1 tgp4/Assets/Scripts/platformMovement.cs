using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platformMovement : MonoBehaviour
{
    public Vector3 posFinal;
    public Vector3 posInicial;
    public float speed;
    IEnumerator Vector3Corrotina(Vector3 target, float speed)
    {
        Vector3 startingPos = gameObject.transform.position;
        float time = 0;
        while (gameObject.transform.position != target)
        {
            gameObject.transform.position = Vector3.Lerp(startingPos, target, (time/Vector3.Distance(startingPos, target)) * speed);
            time += Time.deltaTime;
            yield return null;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        posInicial = gameObject.transform.position;
        StartCoroutine(Vector3Corrotina(posFinal, speed));
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (gameObject.transform.position == posFinal)
        {
            StartCoroutine(Vector3Corrotina(posInicial, speed));
        }
        if (gameObject.transform.position == posInicial)
        {
            StartCoroutine(Vector3Corrotina(posFinal, speed));
        }
    }
}
