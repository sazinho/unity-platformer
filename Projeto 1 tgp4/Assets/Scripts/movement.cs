using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class movement : MonoBehaviour
{
    private Rigidbody2D rb;
    public float jumpForce;
    public float speed;
    public int moeda;
    public Text counterMoeda;
    public bool viradoPraDireita;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(new Vector2(0, jumpForce));
        }
        float dirX = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2 (dirX * speed, rb.velocity.y);
        if (dirX >= 0)
        {
            viradoPraDireita = true;
        }
        else if (dirX <= 0)
        {
            viradoPraDireita = false;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Moeda")
        {
            collision.gameObject.SetActive(false);
            moeda++;
            counterMoeda.text = moeda.ToString();
        }
    }
}
